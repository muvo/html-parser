<?php namespace muvo\www\parser;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Request;
use Monolog\Logger;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\UriInterface;
use Psr\Log\LoggerInterface;

class XPath
{
    /**
     * @var Client
     */
    protected $_client;

    /**
     * @var array
     */
    protected $_template;

    /**
     * @var Logger
     */
    public $logger;

    public function __construct(array $template, LoggerInterface $logger = null, ClientInterface $client = null)
    {
        $this->logger = $logger ?? new Logger('www-parser');
        $this->_client = $client ?? new Client();

        $filteredTemplate = [];
        foreach ($template as $key => $value) {
            if (!is_string($key) || (!is_string($value)) && (!is_array($value))) {
                $this->logger->warning("Invalid key or name of template element", [$key, $value]);
                continue;
            }
            $filteredTemplate[$key] = $value;
        }

        $this->logger->info("Parser module is loaded :-)", $this->_template = $filteredTemplate);
    }

    /**
     * @param \DOMXPath $xPath
     * @param array $template
     * @return array
     */
    protected function _parse(\DOMXPath $xPath, array $template)
    {
        $result = array_fill_keys(array_keys($template), null);

        foreach ($template as $key => $path) {
            if (is_string($path)) {
                $this->logger->debug("Querying document with path", ['key' => $key, 'path' => $path]);
                $el = $xPath->query($path);
                $this->logger->debug("OK", ['count' => $el->length]);

                if ($el->length === 1) {
                    $result[$key] = $el->item(0);
                } elseif ($el->length > 1) {
                    for ($i = 0; $i < $el->length; $i++) {
                        $result[$key][$i] = $el->item($i);
                    }
                } else {
                    $this->logger->warning("Can't query defined path", ['key' => $key, 'path' => $path]);
                }
            } elseif (is_array($path)) {
                $this->logger->info(sprintf("Processing recursive template for key «%s»", $key));
                $result[$key] = $this->_parse($xPath, $path);
            }
        }

        return $result;
    }

    /**
     * @param string|UriInterface|RequestInterface $request
     * @param array $httpRequestOptions
     * @return array|null
     * @throws \ErrorException
     */
    public function parse($request, array $httpRequestOptions = [])
    {
        if (is_string($request) || $request instanceof UriInterface) {
            $request = new Request('GET', $request);
        } elseif (!$request instanceof RequestInterface) {
            throw new \ErrorException("URL must be a string or implement UriInterface");
        }

        $this->logger->notice("Trying to request data by URL", ['url' => $request->getUri()->__toString()]);
        try {
            $document = new \DOMDocument();
            libxml_use_internal_errors(true);

            $response = $this->_client->send($request, $httpRequestOptions);
            $document->loadHTML($response->getBody());

            return $this->_parse(new \DOMXPath($document), $this->_template);
        } catch (\Throwable $exception) {
            $this->logger->critical($exception->getMessage());
        }

        return null;
    }
}
