Simple usage example
====================

```php
<?php

require_once __DIR__ . '/vendor/autoload.php';

$parser = new \muvo\www\parser\XPath([
    'kino' => '//*[@id="topls"]/a',
]);


$r = $parser->parse('http://gidonline.club');

print_r($r);
```
